# 43 ms
class Solution:
    # @return a list of strings, [s1, s2]
    def letterCombinations(self, digits):
        numToLetter = dict()
        numToLetter['2'] = ["a","b","c"]
        numToLetter['3'] = ["d","e","f"]
        numToLetter['4'] = ["g","h","i"]
        numToLetter['5'] = ["j","k","l"]
        numToLetter['6'] = ["m","n","o"]
        numToLetter['7'] = ["p","q","r","s"]
        numToLetter['8'] = ["t", "u", "v"]
        numToLetter['9'] = ["w","x","y","z"]
        res = [""]
        for i in range(len(digits)):
            l = numToLetter[digits[i]]
            tempres = []
            for j in range(len(l)):  
                tempres.extend(map((lambda x : x + l[j]), res))
            res = tempres
        return res
