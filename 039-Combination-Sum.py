# 133 ms
class Solution:
    def combinationSum(self, candidates, target):
        res = []
        candidates.sort()
        counter = [0] * len(candidates)
        sums = [0] * len(candidates)
        summed = 0
        while candidates[-1] * counter[-1] <= target:
            i = 0
            while i < len(counter)-1 and target < summed+candidates[i]:
                summed -= sums[i]
                counter[i] = 0
                sums[i] = 0
                i += 1
            counter[i] += 1
            sums[i] += candidates[i]
            summed += candidates[i]
            if summed == target:
                res.append(self.buildList(counter, candidates))
        return res

    def buildList(self, counter, candidates):
        res = []
        for i in xrange(len(counter)):
            res.extend([candidates[i]]*counter[i])
        return res