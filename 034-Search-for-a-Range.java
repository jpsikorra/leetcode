// 239 ms
public class Solution {
    public int[] searchRange(int[] A, int target) {
        int[] result = new int[] {-1, -1};
        int left = 0, mid = 0, right = A.length-1;
        boolean found = false;
        
        while(!found && left < right){
            mid = (left + right) / 2;
            if (A[mid] == target)
                    found = true;
            else if (A[mid] < target)
                    left = mid+1;
            else
                    right = mid-1;   
        }
        if (found || (left < A.length && A[left] == target)) {
            while(A[left] < target)
                left++;
            while(A[right] > target)
                right--;
            result[0] = left;
            result[1] = right;
        }
        return result;
    }
}
