# 240 ms O(n^2 * log n)
class Solution:
    # @return a list of lists of length 4, [[val1,val2,val3,val4]]
    def fourSum(self, num, target):
        num.sort()
                        
        sums = dict()
        for i in range(len(num)-1):
            n = num[i]
            for j in range(i+1,len(num)):
                m = num[j]        
                su = n + m
                if su in sums:
                    sums[su].append((i,j))
                else:
                  sums[su] = [(i,j)]
        
        res = set()        
        keys = sums.keys()
        for k in keys:
            if target-k in sums:
                dxs1 = sums[k]
                dxs2 = sums[target-k]
                for dx1 in dxs1:
                    for dx2 in dxs2:
                        if not dx1[0] == dx2[0] and not dx1[0] == dx2[1]:
                            if  not dx1[1] == dx2[0] and not dx1[1] == dx2[1]:
                                l = [dx1[0], dx1[1], dx2[0], dx2[1]]
                                l.sort()
                                l = [num[x] for x in l]                                
                                res.add(tuple(l))      
                                
        return [list(x) for x in list(res)]
