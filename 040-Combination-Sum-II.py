# 84ms
class Solution:
    def combinationSum2(self, candidates, target):
        candidates.sort()
        rest = dict()
        i = 0
        while i < len(candidates):
            cand = candidates[i]
            # Count duplicates
            j = i+1
            while j < len(candidates) and candidates[j] == cand:
                j += 1
            cands = []
            s = 0
            # Perform calculation and insert for all amounts of duplicates
            newRest = dict()
            for n in xrange(j-i):
                cands = cands + [cand]
                s += cand
                for r in rest.keys():
                    diff = r-s
                    if 0 <= diff:
                        newVs = [l + cands for l in rest[r]]
                        if diff in newRest:
                            newRest[diff].extend(newVs)
                        else:
                            newRest[diff] = newVs
                if 0 <= target-s:
                    if target-s in newRest:
                            newRest[target-s].append(cands)
                    else:
                        newRest[target-s] = [cands]
            for r in newRest.keys():
                if r in rest:
                    rest[r].extend(newRest[r])
                else:
                    rest[r] = newRest[r]
            # Iterate i
            i = j
        # Result
        if 0 in rest:
            return rest[0]
        else:
            return []