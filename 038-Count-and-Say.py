# 50 ms
class Solution:
    # @return a string
    def countAndSay(self, n):
        result = "1"
        for i in range(n-1):
            lastChr = result[0]
            count = 1
            newResult = ""
            for j in range(1,len(result)):
                if lastChr == result[j]:
                    count += 1
                else:
                    newResult += str(count)
                    newResult += str(lastChr)
                    lastChr = result[j]
                    count = 1
            newResult += str(count)
            newResult += str(lastChr)
            result = newResult
        return result
