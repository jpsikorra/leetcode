# 69 ms
class Solution:
    def trap(self, A):
        currentObstacle = 0
        currentObstacleIdx = 0
        currentWaterStones = 0
        currentDist = 0
        water = 0
        i = 0
        while i < len(A) and A[i] == 0:
            i += 1
        if i < len(A):
            currentObstacle = A[i]
            currentObstacleIdx = i
            i += 1
        while i < len(A):
            if A[i] >= currentObstacle:
                water += currentDist * currentObstacle - currentWaterStones
                currentDist = 0
                currentObstacle = A[i]
                currentObstacleIdx = i
                currentWaterStones = 0
            else:
                currentDist += 1
                currentWaterStones += A[i]
            i += 1
        if A != [] and A[i-1] < currentObstacle:
            water += self.trap(A[currentObstacleIdx:i][::-1])
        return water



