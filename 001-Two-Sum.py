# 216 ms
class Solution:
    # @return a tuple, (index1, index2)
    def twoSum(self, num, target):
        d = dict()
        for i in range(0, len(num)):
            if num[i] in d:
                return (d[num[i]]+1,i+1)
            else:
                d[target-num[i]] = i
