public class Solution {

    static final String[] letters = {"M" , "D", "C", "L", "X", "V", "I"};
    static final int[]    values  = {1000, 500, 100, 50 , 10 , 5  , 1  };

        public String intToRoman(int num){
            return intToRomanRec(num, 0);
        }

        private String intToRomanRec(int num, int i) {
        if (num == 0)
            return "";
        String roman = "";
        boolean five = i % 2 == 1;
        boolean used = false;
        while(values[i] <= num && !(used && five)){
            roman += letters[i];
            num   -= values[i];
            used = true;
        }
        if (i != 6){
            int subIdx = five ? i+1 : i+2;
            int left = values[i] - values[subIdx];
            if (left <= num){
                num -= left;
                roman += letters[subIdx] + letters[i];
            }
        }
        return roman + intToRomanRec(num, i+1);
    }
}