// 309 ms O(nm)
// There are way better solutions using KMP-algorithm.
public class Solution{
    public int strStr(String haystack, String needle) {
        if (needle == "")
            return 0;
        for (int i = 0; i <= haystack.length()-needle.length(); i++){
            int j = 0;
            int k = i;
            while (j < needle.length() && haystack.charAt(k) == needle.charAt(j)){
                k++;
                j++;
            }
            if (j == needle.length())
                return i;
        }
        return -1;
    }
}