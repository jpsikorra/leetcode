// 49 ms
class Solution {
public:
    string convert(string s, int nRows) {

      int step, i, j, stringSize, nRowsMinusOne, smallStep, inbetween;
      string res;

      // set step size
      if (nRows <= 1)
        return s;
      else
        step = nRows * 2 - 2;
      stringSize = s.size();
      res = string();

      // First row
      for (j = 0; j < stringSize; j += step)
        res.push_back(s[j]);

      // Inside rows
      nRowsMinusOne = nRows-1;
      for (i = 1; i < nRowsMinusOne; i++){
        smallStep = (nRows - (i+1)) * 2;
        for (j = i; j < stringSize; j += step){
          res.push_back(s[j]);
          inbetween = j + smallStep;
          if (inbetween < stringSize)
            res.push_back(s[inbetween]);
        }
      }
      // Last row
      for (j = nRowsMinusOne; j < stringSize; j += step)
          res.push_back(s[j]);
      return res;
  }
};
