// 206 ms
public class Solution {
    public void nextPermutation(int[] num) {
        int temp, i, j;
        for (i = num.length-1; i > 0 && num[i] <= num[i-1]; i--);
        if (i == 0)
            reverse(num, 0, num.length-1);
        else {
            temp = num[i-1];
            for(j = num.length-1; num[j] <= num[i-1]; j--);
            num[i-1] = num[j];
            num[j] = temp;
            reverse(num,i, num.length-1);
        }
    }

    private void reverse(int[] num, int l, int r){
         int temp;
         for (int i = l; i <= l + (r - l) / 2; i++){
             temp = num[i];
             num[i] = num[l + r-i];
             num[l + r-i] = temp;
         }
    }
}