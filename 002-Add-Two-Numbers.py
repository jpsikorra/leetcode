# 648 ms
class Solution:
  # @return a ListNode
  def addTwoNumbers(self, l1, l2):
    (res,nOf)   = self.adder(l1.val, l2.val, 0)
    initialNode = ListNode(res)
    self.startCycle(initialNode, l1.next, l2.next, nOf)
    return initialNode

  def startCycle(self, lastNode, l1, l2, overflow):
    if l1 == None:
      l1Empty = True
      l1Value = 0
      l1Next  = None
    else:
      l1Empty = False
      l1Value = l1.val
      l1Next  = l1.next
    if l2 == None:
      l2Empty = True
      l2Value = 0
      l2Next  = None
    else:
      l2Empty = False
      l2Value = l2.val
      l2Next  = l2.next
    if overflow == 0:
      overflowEmpty = True
    else:
      overflowEmpty = False
    if l1Empty & l2Empty & overflowEmpty:
      return
    else:
      (res,nOf) = self.adder(l1Value, l2Value, overflow)
      node = ListNode(res)
      lastNode.next = node
      self.startCycle(node, l1Next, l2Next, nOf)

  def adder(self, val1, val2, overflow):
    summ  = val1 + val2 + overflow
    res   = summ % 10
    nOf   = summ // 10
    return (res,nOf)