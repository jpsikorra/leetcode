# 70 ms
class Solution:
    # @param s, a string
    # @return an integer
    def longestValidParentheses(self, s):
        maxL = 0 
        stack = [-1]
        for i in range(len(s)):
            if s[i] == ')':
                if stack != []:
                    stack.pop()
                    if stack == []:
                        stack.append(i)
                    else:
                       maxL = max(maxL, i - stack[-1])
            else:
                stack.append(i)
        return maxL
