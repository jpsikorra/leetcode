// 46 ms
class Solution {
public:
    bool isMatch(const char *s, const char *p){
      if (*p == '\0')
        return *s == '\0';
      if (*(p+1) != '*')
          return isMatchFirst(s, p) && isMatch(s+1,p+1);
      return isMatch(s,p+2) || isMatchFirst(s,p) && isMatch(s+1,p);
    }

    bool isMatchFirst(const char *s, const char *p){
      return *s == *p || *p == '.' && *s != '\0';
    }
};