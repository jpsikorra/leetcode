# 57 ms O(len(needle) *
# Using the Knuth-Morris-Pratt-algorithm
class Solution:
    # @param haystack, a string
    # @param needle, a string
    # @return an integer
    def strStr(self, haystack, needle):
        if needle == "":
            return 0

        shift = self.makeShift(needle)

        nIdx = 0
        for hIdx in range(len(haystack)):
            while 0 <= nIdx and needle[nIdx] != haystack[hIdx]:
                nIdx = shift[nIdx]
            nIdx += 1
            if len(needle) == nIdx:
                return hIdx - len(needle) + 1
        return -1

    def makeShift(self, pattern):
        shift = [-1]
        j = 0
        for i in range(1,len(pattern)):
                shift.append(j)
                if (pattern[i] == pattern[j]):
                    j += 1
                else:
                    j = 0
        return shift