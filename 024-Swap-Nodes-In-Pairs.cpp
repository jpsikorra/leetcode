// 6 ms
class Solution {
public:
    ListNode *swapPairs(ListNode *head) {
        ListNode *first = NULL;
        ListNode *second = NULL;
        ListNode *third = NULL;

        // First swap
        first = head;
        if (first != NULL){
            second = first->next;
            if (second != NULL){
                first->next = second->next;
                second->next = first;
                head = second;
                second = first->next;
            }
        }
        // Loop swap
        while(second != NULL){
            third = second->next;
            if (third != NULL){
                first->next = third;
                second->next = third->next;
                third->next = second;
            }
            first = second;
            second = second->next;
        }
        return head;
    }
};