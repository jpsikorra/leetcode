// 208 ms
public class Solution {
    public int removeElement(int[] A, int elem) {
        int i = 0;
        int j = A.length;
        while(i < j){
            if (A[i] == elem)
                A[i] = A[--j];
            else i++;
        }
        return j;
    }
}