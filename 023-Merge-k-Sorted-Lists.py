# 577
class Solution:
    # @param a list of ListNode
    # @return a ListNode
	def mergeKLists(self, lists):
    		if lists == []:
			return None
		while len(lists) > 1:
			temp = []
			for i in range(0,len(lists)-1,2):
				temp.append(self.mergeLists(lists[i], lists[i+1]))
			if len(lists) % 2 == 1:
				temp.append(lists[len(lists)-1])
			lists = temp
		return lists[0]

	def mergeLists(self, list1, list2):
		if list1 == None:
			return list2
		if list2 == None:
			return list1
		if list1.val < list2.val:
			start = list1
			list1 = list1.next
		else:
			start = list2
			list2 = list2.next
		node = start
		while list1 != None and list2 != None:
			if list1.val < list2.val:
				node.next = list1
				list1 = list1.next
			else:
				node.next = list2
				list2 = list2.next
			node = node.next
		if list1 != None:
			node.next = list1
		if list2 != None:
			node.next = list2
		return start