// 35 ms
class Solution {
public:
    ListNode *reverseKGroup(ListNode *head, int k) {
        ListNode *start = head;
        ListNode *next;

        // First group
        if (!checkLength(start,k))
            return start;
        head = reverseList(start,k);
        next = start->next;

        // Loop while list still holds k elements
        while(checkLength(next,k)){
            start->next = reverseList(next,k);
            start = next;
            next = next->next;
        }
        if (next != NULL){
            start->next = next;
        }
        return head;
    }

    // Check wether the length of the remaining list is longer than k
    bool checkLength(ListNode *start, int k){
        while(k > 0 && start != NULL){
            k--;
            start = start->next;
        }
        return k == 0 ? true : false;
    }

    // Reverse the first k elements of a list of length >= k > 0.
    ListNode *reverseList(ListNode *start, int k){
        ListNode *first, *second, *third;

        // First node
        first = NULL;
        second = start;
        third = start->next;

        // Loop
        for (int i = 1; i < k; i++){
            first = second;
            second = third;
            third = second->next;
            second->next = first;
        }
        start->next = third;
        return second;
    }
};