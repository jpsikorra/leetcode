// 14 ms
class Solution {
public:
    int calculateMinimumHP(vector<vector<int> > &dungeon) {
        int widthMinusOne = dungeon.size()-1;
        int heightMinusOne = dungeon[0].size()-1;
        int j, hpNeededLater, hpNeeded;
        bool bottomFlag, rightEdgeFlag;
        for (int i = widthMinusOne; 0 <= i; i--){
            rightEdgeFlag = i == widthMinusOne ? true : false;
            for (j = heightMinusOne; 0 <= j; j--){
                bottomFlag = j == heightMinusOne ? true : false;
                if (bottomFlag && rightEdgeFlag)
                    hpNeededLater = 0;
                else if (bottomFlag)
                    hpNeededLater = dungeon[i+1][j];
                else if (rightEdgeFlag)
                    hpNeededLater = dungeon[i][j+1];
                else
                    hpNeededLater = min(dungeon[i+1][j], dungeon[i][j+1]);
                hpNeeded = hpNeededLater - dungeon[i][j];
                dungeon[i][j] = hpNeeded < 0 ? 0 : hpNeeded;
            }
        }
        return dungeon[0][0]+1;
    }
};