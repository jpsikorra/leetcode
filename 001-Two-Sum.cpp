class Solution {
public:
    vector<int> twoSum(vector<int> &numbers, int target) {
        unordered_map<int,int> table;
        unordered_map<int,int>::const_iterator it;
        int i = 0;
        int x;
        while(1){
            x = numbers[i];
            it = table.find(x);
            if (it != table.end())
                break;
            table[target - x] = i;
            i++;
        }
        vector<int> result = vector<int>();
        result.push_back(it->second+1);
        result.push_back(i+1);
        return result;
    }
};