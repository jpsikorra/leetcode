// 313 ms
import java.util.HashMap;

public class Solution {

    HashMap<String,Integer> names = new HashMap<String,Integer>(13);

    public Solution(){
        names.put("M" , 1000);
        names.put("CM", 900 );
        names.put("D" , 500 );
        names.put("CD", 400 );
        names.put("C" , 100 );
        names.put("XC",  90 );
        names.put("L" ,  50 );
        names.put("XL",  40 );
        names.put("X" ,  10 );
        names.put("IX",   9 );
        names.put("V" ,   5 );
        names.put("IV",   4 );
        names.put("I" ,   1 );
    }

    public int romanToInt(String s) {
        Integer nextValue;
        int res = 0;
        int i;
        for (i = 0; i < s.length()-1; i++){
            nextValue = names.get(s.substring(i, i+2));
            if (nextValue != null){
                res += nextValue;
                i++;
            }
            else {
                res += names.get(s.substring(i, i+1));
            }
        }
        if (i < s.length()){
            res += names.get(s.substring(i,i+1));
        }
        return res;
    }
}