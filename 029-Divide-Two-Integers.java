// 225 ms
public class Solution {
    public int divide(int dividend, int divisor) {
        if (dividend == Integer.MIN_VALUE && divisor == -1)
            return Integer.MAX_VALUE;
        if (dividend == Integer.MIN_VALUE && divisor == 1)
            return Integer.MIN_VALUE;

        long dii = dividend < 0 ? -(long) dividend : (long) dividend;
        long dis = divisor  < 0 ? -(long) divisor  : (long) divisor ;
        int shiftCount;
        long result = 0;

        for (shiftCount = 0; dis < dii; shiftCount++)
            dis <<= 1;

        while(shiftCount >= 0){
            if (dii >= dis){
                dii -= dis;
                result += 1 << shiftCount;
            }
            dis >>= 1;
            shiftCount--;
        }

        // Compare signs
        return (int) ((dividend < 0) == (divisor < 0) ? result : -result);
    }
}