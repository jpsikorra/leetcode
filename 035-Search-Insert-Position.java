// 225 ms
public class Solution {
    public int searchInsert(int[] A, int target) {
        int left = 0, right = A.length-1, mid;
        while(left <= right){
            mid = (left + right) / 2;
            if (A[mid] == target)
                return mid;
            if (A[mid] < target)
                left = mid+1;
            else
                right = mid-1;
        }
        return left;
    }
}
