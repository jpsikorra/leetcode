// 40 ms
class Solution {
public:
    int atoi(const char *str) {
        bool minusFlag = false;
        // Remove trailing whitespaces
        int i, j;
        for (i = 0; str[i] == ' ' || str[i] == '\t'; i++);;
        switch (str[i]){
            case '\0':
                return 0;
                break;
            case '+':
                i++;
                break;
            case '-':
                minusFlag = true;
                i++;
                break;
            default:
                ;
        }
        long long res = 0;
        for(j = i; str[j] != '\0' && j - i < 11; j++){
            if (str[j] < 48 || 57 < str[j])
                break;
            res *= 10;
            res += str[j] - 48;
        }
        res = minusFlag ? res * (-1) : res;
        if (res < INT_MIN)
            return INT_MIN;
        else if (res > INT_MAX)
            return INT_MAX;
        else
            return (int) res;
    }
};