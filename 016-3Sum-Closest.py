# 418 ms
class Solution:
    # @return an integer
    def threeSumClosest(self, num, target):
        num = qSort(num)
        le = len(num)
        leMinusOne = le-1
        sum = num[0] + num[1] + num[2]
        bestDist = abs(sum - target)
        bestSum  = sum 
        for i in range(0,leMinusOne):
            n = num[i]
            j = i+1
            k = leMinusOne
            while j < k:
                m = num[j]
                o = num[k]
                sum = n + m + o
                dist = abs(sum - target)
                if dist < bestDist:
                    bestDist = dist
                    bestSum = sum
                if sum < target:
                    j += 1
                else:
                    k -= 1
        return bestSum
        
        
    def qSort(self, num):
        if len(num) <= 1:
            return num
        else:
            pivot = num[0]
            left = [n for n in num[1:] if n <= pivot]
            right = [n for n in num[1:] if n > pivot]
            return self.qSort(left) + [pivot] + self.qSort(right)       
        
