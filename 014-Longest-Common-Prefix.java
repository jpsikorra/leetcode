// 319 ms
public class Solution {
    public String longestCommonPrefix(String[] strs) {
        char c = '\0';
        boolean flag = true;
        String prefix = "";
        boolean notEmpty = 0 < strs.length;
        int j;
        for (int i = 0; flag && notEmpty && i < strs[0].length(); i++){
            c = strs[0].charAt(i);
            for (j = 1; j < strs.length && flag; j++){
                if (strs[j].length() <= i || strs[j].charAt(i) != c)
                        flag = false;
                }
            if (flag)
                prefix += Character.toString(c);
        }
        return prefix;
    }
}