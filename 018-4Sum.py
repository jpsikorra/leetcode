# 1600 ms O(n^3)
class Solution:
    # @return a list of lists of length 4, [[val1,val2,val3,val4]]
    def fourSum(self, num, target):
        # Sort nums O(n^2)
        num.sort()

        res = set()

        le = len(num)
        # Find possible solutions O(n^3)
        for i in range(le-2):
            m = num[i]
            t = target - m
            # Three sum algorithm O(n^2)
            for j in range(i+1,le-1):
                k = j+1
                l = le-1
                n = num[j]
                while k < l:
                 o  = num[k]
                 p  = num[l]
                 su = n + o + p
                 if su == t:
                    res.add((m, n, o, p))
                    k = k+1
                 elif su < t:
                    k = k+1
                 else:
                    l = l-1
        return [list(x) for x in list(res)]
