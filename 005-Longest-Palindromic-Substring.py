# 1826 ms
class Solution:
    # @return a string
    def longestPalindrome(self, s):
        # Naive algorithm
        lengthLongestPali = 0
        longestPaliStart = 0
        longestPaliEnd   = 0
        i = 0
        lengthOfS = len(s)
        while i < lengthOfS:
          j = i-1
          k = i+1
          while j >= 0 and k < lengthOfS and s[j] == s[k]:
            j -= 1
            k += 1
          paliLength = k - j - 1
          if lengthLongestPali < paliLength:
            lengthLongestPali = paliLength
            longestPaliStart = j+1
            longestPaliEnd   = k
          j = i-1
          k = i
          while j >= 0 and k < lengthOfS and s[j] == s[k]:
            j -= 1
            k += 1
          paliLength = k - j - 1
          if lengthLongestPali < paliLength:
            lengthLongestPali = paliLength
            longestPaliStart = j+1
            longestPaliEnd   = k
          i += 1
        return s[longestPaliStart:longestPaliEnd]