// 309 ms
// This has runtime complexity of O(n^3).
// A better solution is described at
// http://leetcode.com/2010/04/finding-all-unique-triplets-that-sums.html
public class Solution {
    public List<List<Integer>> threeSum(int[] num) {

      List<List<Integer>> result = new ArrayList<List<Integer>>();
      List<Integer> temp;
      int len = num.length;

      Arrays.sort(num);

      int i, I, j, J, k, K;
      for (i = 0; i < len-2; i = I){
        for (j = i+1; j < len-1; j = J){
          for (k = j+1; k < len; k = K){
            if (num[i] + num[j] + num[k] == 0){
              temp = new ArrayList<Integer>(3);
              temp.add(num[i]);
              temp.add(num[j]);
              temp.add(num[k]);
              result.add(temp);    
            }
            for (K = k+1; K < len && num[K] == num[k]; K++);             
          }
          for (J = j+1; J < len-1 && num[J] == num[j]; J++);
        }
        for (I = i+1; I < len-2 && num[I] == num[i]; I++);
      }
      return result;
    }
}
