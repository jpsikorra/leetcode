// 6 ms
class Solution {
public:
    vector<string> generateParenthesis(int n) {
        vector<string> results = vector<string>();
        addPars(&results, "", n, 0);
        return results;
    }

    void addPars(vector<string> *results, string s, int n, int m){
        if (n == 0 && m == 0) results->push_back(s);
        if (n > 0) addPars(results, s + "(", n-1, m+1);
        if (m > 0) addPars(results, s + ")", n, m-1);
    }
};