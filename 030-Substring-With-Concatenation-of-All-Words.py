# 139 ms
class Solution:
    def findSubstring(self, S, L):
        jump = len(L[0])
        seqLen = len(L) * jump
        
        di = dict()
        hashsum = 0
        i = 1
        for w in L:
            if di.get(w) != None:
                hashsum = hashsum + di[w]
            else:
                di[w] = hash(w)
                hashsum = hashsum + di[w]
                i = i+1
            
        hashes = []
        for i in range(len(S)):
            if (di.get(S[i:i+jump]) == None):
                hashes.append(0)
            else:
                hashes.append(di[S[i:i+jump]])
        
        result = []
        for i in xrange(len(S)-seqLen+1):
            if sum(hashes[i:i+seqLen:jump])==hashsum:
                result.append(i)
        return result
