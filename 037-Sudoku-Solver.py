# 219 ms
class Solution:
    # Solves a Sudoku.
    # Input is a list of lists of characters.
    # The result is saved in the input list.
    def solveSudoku(self, board):
        possBoard = []
        for i in xrange(9):
            line = []
            for j in xrange(9):
                if board[i][j] == '.':     
                    line.append(['1','2','3','4','5','6','7','8','9']) 
                else:
                    line.append(None)
            possBoard.append(line)
        self.backtrack(board, possBoard)

    # Basic backtracking, bro.
    def backtrack(self, board, oPossBoard):
        possBoard = self.findPossibleNumsForBoard(board, oPossBoard)
        minPoss = 10
        for i in xrange(9):
            for j in xrange(9):
                if possBoard[i][j] != None and len(possBoard[i][j]) < minPoss:
                    x = i
                    y = j
                    minPoss = len(possBoard[i][j])
        if minPoss == 10:
            return True
        for c in possBoard[x][y]:
            board[x][y] = c
            if self.backtrack(board, possBoard):
                return True
        board[x][y] = '.'
        return False

    # Finds all numbers for all fields that are still possible.
    def findPossibleNumsForBoard(self, board, oPossBoard):
        possBoard = []
        for i in xrange(9):
            line = []
            for j in xrange(9):
                if board[i][j] == '.':
                    line.append(self.findPossibleNums(board, i, j, oPossBoard))
                else:
                    line.append(None)
            possBoard.append(line)
        return possBoard

    # Finds all numbers that are still possible for one field.
    def findPossibleNums(self, board, x, y, possBoard):
        possibleNums = []
        sqX = (x // 3) * 3
        sqY = (y // 3) * 3
        for n in possBoard[x][y]:
            if self.isPossibleNum(board, x, y, sqX, sqY, n):
                possibleNums.append(n)
        return possibleNums 
 
    # Check wether a number is possible for a field
    def isPossibleNum(self, board, x, y, sqX, sqY, n):
        for i in xrange(9):
            if (board[i][y] == n) or (board[x][i] == n):
                return False
        for i in xrange(3):
            for j in xrange(3):
                if board[sqX+i][sqY+j] == n:
                    return False
        return True
