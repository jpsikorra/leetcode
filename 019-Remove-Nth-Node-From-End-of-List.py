# 52 ms O(n)
# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    # @return a ListNode
    def removeNthFromEnd(self, head, n):
        # Count
        node = head
        length = 1
        while node.next is not None:
            length += 1
            node = node.next
        return self.removeNth(head, length - n)

    def removeNth(self, head, ndx):
        if ndx == 0:
            return head.next
        node = head
        for i in range(ndx-1):
            node = node.next
        node.next = node.next.next
        return head