// 205 ms
public class Solution{
    public int search(int[] A, int target){
        if (A.length == 0)
            return -1;
        int smallIdx = findSmallest(A);
        if (smallIdx == 0)
            return find(A,0,A.length-1,target);
        if (A[0] > target)
            return find(A,smallIdx,A.length-1,target);
        else
            return find(A,0,smallIdx-1,target);
    }

    public int findSmallest(int[] A){
        int left = 0;
        int right = A.length-1;
        int x = 0;
        while (left < right){
            x = (left + right) / 2;
            if (A[x] > A[right])
                    left = x+1;
            else
                    right = x;      
        }
        return left;
    }
   
    public int find(int[] A, int start, int end, int target){
        if (start > end) return -1;
        int idx = 0;
        while (start < end){
            idx = start + Math.max((end - start) / 2,1);
            if (A[idx] == target)
                return idx;
            if (A[idx] < target)
                start = idx;
            else
                end   = idx-1;
            }
        return A[end] == target ? end: -1; 
    }
}   
