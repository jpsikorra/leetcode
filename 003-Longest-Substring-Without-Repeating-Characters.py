# 382 ms
class Solution:
    # @return an integer
    def lengthOfLongestSubstring(self, s):
        readChars = dict()
        maxLength = 0
        curIndex  = 1
        lastIdx   = 1
        while s is not "":
            c, s = s[0], s[1:]

            if c in readChars:
                lengthOfSub = curIndex - lastIdx
                if maxLength < lengthOfSub:
                    maxLength = lengthOfSub

                lastCIdx = readChars[c]
                lastIdx = max((lastIdx, lastCIdx+1))

            readChars[c] = curIndex
            curIndex += 1
        if maxLength < curIndex - lastIdx:
            maxLength = curIndex - lastIdx
        return maxLength