# 36 ms
class Solution:
    # @return a boolean
    def isValid(self, s):
        stack = []
        di = dict([(')','('),(']','['),('}','{')])
        k = [')',']','}']
        for c in s:
            if c in k:
                if stack == [] or di[c] != stack.pop():
                    return False
            else:
                stack.append(c)
        return stack == []
