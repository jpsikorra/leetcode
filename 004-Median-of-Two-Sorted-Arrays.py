# 612 ms
class Solution:
    # @return a float
    def findMedianSortedArrays(self, A, B):
      # to reduce cases, use the bigger collection as B
      if  len(A) > len(B):
        return self.fMSAHelper(B, A)
      else:
        return self.fMSAHelper(A, B)

    def fMSAHelper(self, A, B):
      lengA = len(A)
      lengB = len(B)
      if    lengA <= 2:
        for i in range(0, lengA):
          B = self.inserter(B, 0, len(B), A[i])
        return self.findMedian(B)
      else:
        medianA = self.findMedian(A)
        medianB = self.findMedian(B)
        sliceL = lengA // 2
        if lengA % 2 == 1:
          if medianA < medianB:
            del A[0:sliceL]
            del B[lengB-sliceL:lengB]
          else:
            del A[sliceL+1:lengA]
            del B[0:sliceL]
        else:
          if medianA < medianB:
            del A[0:sliceL-1]
            del B[lengB-sliceL+1:lengB]
          else:
            del A[sliceL+1:lengA]
            del B[0:sliceL-1]
        return self.fMSAHelper(A,B)

    def findMedian(self, Array):
      leng = len(Array)
      if    leng == 0:
        raise Error("Bad length of Array 0")
      elif  leng % 2 == 1:
        return Array[leng // 2]
      elif  leng % 2 == 0:
        return (Array[leng // 2 - 1] + Array[leng // 2]) / 2.0

    def inserter(self, B, Start, End, i):
      if Start == End:
        B.insert(Start,i)
        return B
      elif End - Start == 1:
        if B[Start] < i:
          B.insert(End,i)
        else:
          B.insert(Start,i)
        return B
      else:
        medianB = self.findMedian(B[Start:End])
        if i < medianB:
          return self.inserter(B, Start, End - (End - Start) // 2, i)
        else:
          return self.inserter(B, Start + (End - Start) // 2, End, i)