// 300 ms
public class Solution {
    public boolean isValidSudoku(char[][] board) {
        return checkLines(board) && checkSubs(board);  
    }

    private boolean checkLines(char[][] board) {
        boolean bucket1[], bucket2[];
        int i, j, atIxJ, atJxI;
        for (i = 0; i < 9; i++){
            bucket1 = new boolean[9];
            bucket2 = new boolean[9];
            for (j = 0; j < 9; j++){
                atIxJ = (int) board[i][j] - 49;
                atJxI = (int) board[j][i] - 49;
                if (atIxJ >= 0){
                    if (bucket1[atIxJ]) return false;
                    bucket1[atIxJ] = true;
                }
                if (atJxI >= 0){
                    if (bucket2[atJxI]) return false;
                    bucket2[atJxI] = true;
                }
            }
        }
        return true;
    }

    private boolean checkSubs(char[][] board){
        boolean bucket[][][] = new boolean[3][3][9];
        int i, j, atIxJ;
        for (i = 0; i < 9; i++){
            for (j = 0; j < 9; j++){
                atIxJ = (int) board[i][j] - 49;
                if (atIxJ >= 0){
                    if (bucket[i/3][j/3][atIxJ]) return false;
                    bucket[i/3][j/3][atIxJ] = true;
                }
            }
        }
        return true;
    }
}
