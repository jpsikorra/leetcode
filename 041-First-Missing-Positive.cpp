// 4 ms
class Solution {
public:
    int firstMissingPositive(int A[], int n) {
        int temp, i;
        for (i = 0; i < n; i++){
            if (A[i] == 0) A[i] = -1;
        }
        for (i = 0; i < n; i++){
            if (0 < A[i] && A[i] <= n){
                if (A[i]-1 == i) A[i] = 0;
                else if (A[A[i]-1] != 0) {
                    temp = A[A[i]-1];
                    A[A[i]-1] = 0;
                    A[i] = temp;
                    i--;
                }
            }
        }
        for (i = 0; i < n && A[i] == 0; i++);
        return i+1;
    }
};